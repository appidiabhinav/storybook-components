import Render from "./components/Render";

import "./App.css";

function App() {
  return (
    <div>
      <Render />
    </div>
  );
}

export default App;
