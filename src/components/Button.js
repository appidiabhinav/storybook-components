import React from "react";

function Button(props) {
  return (
    <button
      type="button"
      class="py-2 px-4  bg-indigo-600 hover:bg-indigo-700 focus:ring-indigo-500 focus:ring-offset-indigo-200 text-white w-30  transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg mt-5"
      onClick={props.addList}
    >
      Add List
    </button>
  );
}

export default Button;
