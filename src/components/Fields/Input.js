import React from "react";

/**
 * Return JSX
 * @props given value will be string
 * @returns InputField component
 */
function Input(props) {
  let { label, placeHolder, type } = props;
  return (
    <div class=" relative ">
      <label for="name-with-label" class="text-gray-700">
        {label}
      </label>
      <input
        type={type}
        id="name-with-label"
        class=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
        name={label.toLowerCase()}
        placeholder={placeHolder}
      />
    </div>
  );
}

export default Input;
