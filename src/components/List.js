import React, { useState } from "react";
import Button from "./Button";

let dummyData = [
  {
    name: "Jean Marc",
    profession: "Developer",
    time: "6:00 AM",
  },
];

const List = (props) => {
  const [list, setList] = useState(dummyData);

  const addList = () => {
    let addEmployee = {
      name: "Martin",
      profession: "Accountant",
      time: "7:00 AM",
    };
    let updatedList = [...list, addEmployee];
    setList(updatedList);
  };
  let buttonProps = {
    addList: addList,
  };
  return (
    <div>
      <div class="flex flex-col text-center font-semibold text-3xl mt-8 mb-8">
        Tailwind List Component
      </div>

      <div class="container flex flex-col mx-auto w-full items-center justify-center">
        <ul class="flex flex-col">
          {list?.map((item) => {
            return (
              <li class="border-gray-400 flex flex-row mb-2">
                <div class="shadow border select-none cursor-pointer bg-white dark:bg-gray-800 rounded-md flex flex-1 items-center p-4">
                  <div class="flex flex-col w-10 h-10 justify-center items-center mr-4">
                    <img
                      alt="profil"
                      src="/em.jpg"
                      class="mx-auto object-cover rounded-full h-10 w-10 "
                    />
                  </div>
                  <div class="flex-1 pl-1 md:mr-16">
                    <div class="font-medium dark:text-white">{item.name}</div>
                    <div class="text-gray-600 dark:text-gray-200 text-sm">
                      {item.profession}
                    </div>
                  </div>
                  <div class="text-gray-600 dark:text-gray-200 text-xs">
                    {item.time}
                  </div>
                </div>
              </li>
            );
          })}
        </ul>
      </div>
      <div class="flex place-content-center">
        <Button {...buttonProps} />
      </div>
    </div>
  );
};

export default List;
