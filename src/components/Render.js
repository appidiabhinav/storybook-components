import React, { useState } from "react";
import SocialButton from "./Buttons/SocialButton";
import Input from "./Fields/Input";

const Render = (props) => {
  //preparing facebookButton props
  let facebookButtonProps = {
    name: "Sign in with Facebook",
    color: "blue",
    icon:
      "M1343 12v264h-157q-86 0-116 36t-30 108v189h293l-39 296h-254v759h-306v-759h-255v-296h255v-218q0-186 104-288.5t277-102.5q147 0 228 12z",
  };

  //preparing googleButton props
  let googleButtonProps = {
    name: "Sign in with Google",
    color: "red",
    icon:
      "M896 786h725q12 67 12 128 0 217-91 387.5t-259.5 266.5-386.5 96q-157 0-299-60.5t-245-163.5-163.5-245-60.5-299 60.5-299 163.5-245 245-163.5 299-60.5q300 0 515 201l-209 201q-123-119-306-119-129 0-238.5 65t-173.5 176.5-64 243.5 64 243.5 173.5 176.5 238.5 65q87 0 160-24t120-60 82-82 51.5-87 22.5-78h-436v-264z",
  };

  //preparing emailField props
  let emailInputFieldProps = {
    label: "Email",
    placeHolder: "email",
    type: "text",
  };

  //preparing passwordField props
  let passwordInputFieldProps = {
    label: "Password",
    placeHolder: "password",
    type: "text",
  };

  return (
    <div>
      <div class="flex flex-col text-center font-semibold text-3xl mt-8 mb-8">
        Tailwind Components
      </div>

      <div class="flex place-content-center">
        <SocialButton {...facebookButtonProps} />
      </div>
      <div class="flex place-content-center mt-3">
        <SocialButton {...googleButtonProps} />
      </div>
      <div class="flex place-content-center mt-3">
        <Input {...emailInputFieldProps} />
      </div>
      <div class="flex place-content-center mt-3">
        <Input {...passwordInputFieldProps} />
      </div>
    </div>
  );
};

export default Render;
