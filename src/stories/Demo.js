import React from "react";
import PropTypes from "prop-types";

export const Demo = ({ title }) => (
  <article>
    <section>
      <h2>Demos in Storybook</h2>
      <p>
        Render Demos with mock data. This makes it easy to build and review Demo
        states without needing to navigate to them in your app. Here are some
        handy patterns for managing Demo data in Storybook:
      </p>
    </section>
  </article>
);
// Demo.propTypes = {
//   user: PropTypes.shape({}),
//   onLogin: PropTypes.func.isRequired,
//   onLogout: PropTypes.func.isRequired,
//   onCreateAccount: PropTypes.func.isRequired,
// };

Demo.defaultProps = {
  title: "",
};
