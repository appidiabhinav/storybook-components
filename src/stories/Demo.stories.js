import React from "react";

import { Demo } from "./Demo";
import * as HeaderStories from "./Header.stories";

export default {
  title: "Example/Demo",
  component: Demo,
};

const Template = (args) => <Demo {...args} />;

export const Title = Template.bind({});
Title.args = {
  title: "Welcome",
};
